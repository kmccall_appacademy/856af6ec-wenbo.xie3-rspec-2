def reverser(&str)
  str.call.split.map(&:reverse).join(" ")
end

def adder(n1=1, &n2)
  n1 + n2.call
end

def repeater(n=0, &add_one)
  return add_one.call if n == 0

  n.times { |n| add_one.call }
end
