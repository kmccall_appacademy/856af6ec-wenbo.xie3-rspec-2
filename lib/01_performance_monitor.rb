def measure(times = 1, &prc)
  fake_time = Time.now
  times.times { prc.call }
  (Time.now - fake_time) / times
end
